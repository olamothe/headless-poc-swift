//
//  headless.swift
//  headless-poc-swift
//
//  Created by Olivier Lamothe on 2020-03-05.
//  Copyright © 2020 Olivier Lamothe. All rights reserved.
//

import Foundation
import JavaScriptCore

extension JSContext {
    subscript(key: String) -> Any {
        get {
            return self.objectForKeyedSubscript(key)
        }
        set{
            self.setObject(newValue, forKeyedSubscript: key as NSCopying & NSObjectProtocol)
        }
    }
}

@objc protocol JSConsoleExports: JSExport {
    static func log(_ msg: String)
}

class JSConsole: NSObject, JSConsoleExports {
    class func log(_ msg: String) {
        print(msg)
    }
}

@objc protocol JSPromiseExports: JSExport {
    func then(_ resolve: JSValue) -> JSPromise?
    func `catch`(_ reject: JSValue) -> JSPromise?
}

class JSPromise: NSObject, JSPromiseExports {
    var resolve: JSValue?
    var reject: JSValue?
    var next: JSPromise?
    var timer: Timer?
    
    func then(_ resolve: JSValue) -> JSPromise? {
        self.resolve = resolve
        
        self.next = JSPromise()
        
        self.timer?.fireDate = Date(timeInterval: 1, since: Date())
        self.next?.timer = self.timer
        self.timer = nil
        
        return self.next
    }
    
    func `catch`(_ reject: JSValue) -> JSPromise? {
        self.reject = reject
        
        self.next = JSPromise()
        
        self.timer?.fireDate = Date(timeInterval: 1, since: Date())
        self.next?.timer = self.timer
        self.timer = nil
        
        return self.next
    }
    
    func fail(error: String) {
        if let reject = reject {
            reject.call(withArguments: [error])
        } else if let next = next {
            next.fail(error: error)
        }
    }
    
    func success(value: Any?) {
        guard let resolve = resolve else { return }
        var result:JSValue?
        if let value = value  {
            result = resolve.call(withArguments: [value])
        } else {
            result = resolve.call(withArguments: [])
        }

        guard let next = next else { return }
        if let result = result {
            if result.isUndefined {
                next.success(value: nil)
                return
            } else if (result.hasProperty("isError")) {
                next.fail(error: result.toString())
                return
            }
        }
        
        next.success(value: result)
    }
}

extension JSContext {
    static var plus:JSContext? {
        let jsMachine = JSVirtualMachine()
        guard let jsContext = JSContext(virtualMachine: jsMachine) else {
            return nil
        }
        
        jsContext.evaluateScript("""
            Error.prototype.isError = () => {return true}
        """)
        jsContext["console"] = JSConsole.self
        jsContext["self"] = jsContext.globalObject
        
        let fetch:@convention(block) (String)->JSPromise? = { link in
            let promise = JSPromise()
            promise.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) {timer in
                timer.invalidate()
                
                if let url = URL(string: link) {
                    URLSession.shared.dataTask(with: url){ (data, response, error) in
                        if let error = error {
                            promise.fail(error: error.localizedDescription)
                        } else if
                            let data = data,
                            let string = String(data: data, encoding: String.Encoding.utf8) {
                            promise.success(value: string)
                        } else {
                            promise.fail(error: "\(url) is empty")
                        }
                        }.resume()
                } else {
                    promise.fail(error: "\(link) is not url")
                }
            }
            
            return promise
        }
        jsContext["fetch"] = unsafeBitCast(fetch, to: JSValue.self)
        
        return jsContext
    }
}



enum HeadlessFileLoadError: Error {
    case failToRead
    case notFound
}

class HeadlessWrapper {
    var api: JSValue!
    init() throws {
        self.api = nil
        let scriptContent = try? loadFile()
        guard let jsContext = JSContext.plus else {exit(-1)}
        jsContext.evaluateScript("var console = { log: function(message) { _consoleLog(message) } }")
        let consoleLog: @convention(block) (String) -> Void = { message in
            print("console.log: " + message)
        }
        jsContext.setObject(unsafeBitCast(consoleLog, to: AnyObject.self), forKeyedSubscript: "_consoleLog" as (NSCopying & NSObjectProtocol))
        
        jsContext.exceptionHandler = { context, value in
            let stacktrace = value?.objectForKeyedSubscript("stack").toString()
            // type of Number
            let lineNumber = value?.objectForKeyedSubscript("line")
            // type of Number
            let column = value?.objectForKeyedSubscript("column")
            let moreInfo = "in method \(stacktrace)Line number in file: \(lineNumber), column: \(column)"
            print("JS ERROR: \(value) \(moreInfo)")
            
        }
        jsContext.evaluateScript(scriptContent)
        jsContext.evaluateScript("console.log('hello')")
        
        

        let apiClass = jsContext.objectForKeyedSubscript("headless")?.objectForKeyedSubscript("API")
        
       
 
        self.api = apiClass?.construct(withArguments: [])
        
    }

    
    public func fetchNames() {
        print(self.api.objectForKeyedSubscript("fetchNames"))
        print(self.getNames())
        self.api.objectForKeyedSubscript("fetchNames")?.call(withArguments: [])
    }
    
    public func store() -> JSValue! {
        return self.api.objectForKeyedSubscript("store")
    }
    
    public func getState() -> JSValue! {
        return self.store()?.objectForKeyedSubscript("getState")?.call(withArguments: [])
    }
    
    public func getNames() -> [Any]! {
        return self.getState().objectForKeyedSubscript("names")?.toArray()
       }
    
    private func loadFile() throws -> String {
        if let filepath = Bundle.main.path(forResource: "headless2", ofType: "js") {
               do {
                   let content = try String(contentsOfFile: filepath)
                   return content
               } catch {
                   throw HeadlessFileLoadError.failToRead
                   
               }
           } else {
               throw HeadlessFileLoadError.notFound
           }
    }
}

