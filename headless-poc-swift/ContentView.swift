//
//  ContentView.swift
//  headless-poc-swift
//
//  Created by Olivier Lamothe on 2020-03-05.
//  Copyright © 2020 Olivier Lamothe. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        let first = Name(text: "john")
        let second = Name(text: "doe")
        
        
        return VStack(alignment: HorizontalAlignment.center) {
            Button(action:{
                headlessWrapper?.fetchNames()
                
            }) {
                Text("Fetch names")
            }
            List(names) { name in
                NameRow(name: name)
            }
        }
    }
    
}

struct Name: Identifiable {
    var id = UUID()
    var text: String
}

struct NameRow: View {
    var name: Name
    
    var body: some View {
        Text("\(name.text)")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
